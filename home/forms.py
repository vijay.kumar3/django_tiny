from django import forms
from .models import TinyTest


class FooForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(FooForm, self).__init__(*args, **kwargs)
        self.fields['content'].widget.attrs['class'] = 'tiny-class'

    class Meta:
        model = TinyTest
        fields = '__all__'  # your fields here
