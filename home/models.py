from django.db import models


# Create your models here.
class TinyTest(models.Model):
    cat = models.CharField(max_length=100)
    sub_category = models.CharField(max_length=100)
    content = models.TextField()
