from django.contrib import admin
from .models import TinyTest
from .forms import FooForm


# Register your models here.
class FooAdmin(admin.ModelAdmin):
    form = FooForm

    # regular stuff
    class Media:
        js = (
            # 'https://cdn.tiny.cloud/1/ja5o7n4diwjg1yq68mks0gm0len01v8r22c1e1iljiutg2rc/tinymce/5/tinymce.min.js',  # tinymce js file
            'js/tinymce.min.js',
            'js/myscript.js',  # project static folder
        )


admin.site.register(TinyTest, FooAdmin)
