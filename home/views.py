from django.shortcuts import render
from django.views.generic import TemplateView
from .models import TinyTest


# Create your views here.
class TestView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["test"] = TinyTest.objects.get(id=1)
        return context
